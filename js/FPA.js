/*
    Copyright 2011, Wilian Stancke
    Este programa é um software livre; você pode redistribui-lo e/ou 
    modifica-lo dentro dos termos da Licença Pública Geral GNU como 
    publicada pela Fundação do Software Livre (FSF); na versão 2 da 
    Licença.
*/
var fpa;
var FPA = jQuery.Class.create({
	init: function()
	{
		fpa = this;
		
		$(document).ready(function(){
			$("#tabs").tabs();
			
			$(".calendario").datepicker({
				changeYear: true,
				changeMonth: true,
				yearRange: '1900:2010'
			});
			fpa.dialogs();
			fpa.buttons();
		});
	},

	dialogs: function()
	{
		$( ".dialogs_fatores" ).dialog({
			autoOpen: false,
			width: "500px"
		});
		$( ".dialogs" ).dialog({
			autoOpen: false,
			width: "500px"
		});
		$( "#dialog_sobre" ).dialog({
			autoOpen: false,
			width: "300px",
			resizable: false
		});
		$( "#dialog_configuracao" ).dialog({
			autoOpen: false,
			width: "300px",
			resizable: false
		}); 

		
	},
	
	buttons: function()
	{
		

		$("button#salvar").button({
			icons: {
				primary: "ui-icon-disk"
			}
		}).click(function(){
			alert("click");
			return false;
		});

		$( "#config" ).live("click",function() {
			$( "#dialog_configuracao" ).dialog( "open" );
			return false;
		});
		$( "#sobre" ).live("click",function() {
			$( "#dialog_sobre" ).dialog( "open" );
			return false;
		});

		$( ".help-ali" ).live("click",function() {
			$( "#dialog_ali" ).dialog( "open" );
			return false;
		});

		$( ".help-aie" ).live("click",function() {
			$( "#dialog_aie" ).dialog( "open" );
			return false;
		});
		
		$( "#help-sist-dist" ).live("click",function() {
			$( "#dialog_sistema_distribuido" ).dialog( "open" );
			return false;
		});
		
		/** ALI */
		
		$("button#add-ali").button({
			icons: {
				primary: "ui-icon-plusthick"
			}
		}).click(function(){
			$("fieldset.ali:first").clone().insertBefore("#add-ali").slideDown("slow");
			return false;
		});
		
		$("a.del-ali").live("click", function(){
			$(this).parents("fieldset").fadeOut("slow", function(){
				$(this).remove();
			});
		});

		/** AIE */
		$("button#add-aie").button({
			icons: {
				primary: "ui-icon-plusthick"
			}
		}).click(function(){
			$("fieldset.aie:first").clone().insertBefore("#add-aie").slideDown("slow");
			return false;
		});
		
		$("a.del-aie").live("click", function(){
			$(this).parents("fieldset").fadeOut("slow", function(){
				$(this).remove();
			});
		});

		
		/** EE */		
		$("button#add-ee").button({
			icons: {
				primary: "ui-icon-plusthick"
			}
		}).click(function(){
			$("fieldset.ee:first").clone().insertBefore("#add-ee").slideDown("slow");
			return false;
		});
		
		$("a.del-ee").live("click", function(){
			$(this).parents("fieldset").fadeOut("slow", function(){
				$(this).remove();
			});
		});

		/** SE */		
		$("button#add-se").button({
			icons: {
				primary: "ui-icon-plusthick"
			}
		}).click(function(){
			$("fieldset.se:first").clone().insertBefore("#add-se").slideDown("slow");
			return false;
		});
		
		$("a.del-se").live("click", function(){
			$(this).parents("fieldset").fadeOut("slow", function(){
				$(this).remove();
			});
		});

		/** CE */		
		$("button#add-ce").button({
			icons: {
				primary: "ui-icon-plusthick"
			}
		}).click(function(){
			$("fieldset.ce:first").clone().insertBefore("#add-ce").slideDown("slow");
			return false;
		});
		
		$("a.del-ce").live("click", function(){
			$(this).parents("fieldset").fadeOut("slow", function(){
				$(this).remove();
			});
		});

		$(".select-fatores-tecnicos, .select-fatores-ambientais").change(function(){
			
			var peso = parseFloat($(this).parent().next().html());
			var classificacao = parseInt($(this).val());
			var valor =  peso * classificacao;
			$(this).parent().next().next().html(valor);
		});

		$("#resultado").click(function(){
			
			var soma_atores = 0;
			$(".select-ator").each(function(index){
				if(index != 0){
					soma_atores += parseInt($(this).val());
				}
			});
			var soma_casos_de_uso = 0;
			$(".select-caso-de-uso").each(function(index){
				if(index != 0){
					soma_casos_de_uso += parseInt($(this).val());
				}
			});

			var soma_fator_tecnico = 0;
			$(".classificacao-ponderada-fat-tecnicos").each(function(){
				soma_fator_tecnico += parseFloat($(this).html());

			});
			var soma_fator_ambiental = 0;
			$(".classificacao-ponderada-fat-ambiental").each(function(){
				soma_fator_ambiental += parseFloat($(this).html());

			});

			var TCF = 0.6 + (0.01 * soma_fator_tecnico);
			var EF = 1.4 + (-0.03 * soma_fator_ambiental);

			var UUCP = soma_atores + soma_casos_de_uso; 
			var UCP = UUCP * TCF * EF;
			UCP = ((Math.round(UCP*100))/100);
			var HH = UCP * parseInt($("#hora-por-ucp").val());
			var preco_por_ponto = parseInt($("#preco_por_ponto").val());
			
			TCF = ((Math.round(TCF*100))/100);
			EF = ((Math.round(EF*100))/100);
			HH = parseInt(((Math.round(HH*100))/100));

			$("#ucp_sem_ajuste").val(UUCP);
			$("#result_fator_tecnico").val(TCF);
			$("#result_fator_ambiental").val(EF);
			$("#ucp_ajustado").val(UCP);
			$("#homem_hora").val(HH);
			$("#preco_projeto").val(((Math.round((UCP * preco_por_ponto)*100))/100));

			
			

		});
		
		
	}
});
